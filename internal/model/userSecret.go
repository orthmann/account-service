package model

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type UserSecret struct {
	*gorm.Model
	UserId   string `gorm:"user_id"`
	SecretId string `gorm:"secret_id"`
}

func GetUserSecretIdDbEntry(db *gorm.DB, id string) *UserSecret {
	var secret UserSecret
	setSchema(db).Where("user_id=?", id).First(&secret)
	return &secret
}

func CreateUserSecretDbEntry(db *gorm.DB, id string, secretId string) {
	var secret = UserSecret{SecretId: secretId, UserId: id}
	setSchema(db).Create(&secret)
}

func CreateUserSecretId(userId string) string {
	return uuid.New().String()
}
