package services

import (
	"encoding/json"
	"fmt"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"io"
	"net/http"
)

type NilType struct {
	isNil bool
}

func handleResponse[T any](response *http.Response, dataAs *T) (*T, error) {
	if response.StatusCode >= http.StatusOK && response.StatusCode < http.StatusMultipleChoices {
		if _, ok := any(dataAs).(*NilType); ok {
			return nil, nil
		}
		data, err := io.ReadAll(response.Body)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(data, dataAs)
		if err != nil {
			return nil, err
		}
		return dataAs, nil
	} else {
		return nil, &common.ErrorResp{
			Err:  fmt.Errorf("error received from external service. %s", response.Status),
			Code: response.StatusCode,
		}
	}
}
