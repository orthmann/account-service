package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/core/types"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/services"
	"io"
	"net/http"
	"time"
)

type DID struct {
	Id        string    `json:"id"`
	Did       string    `json:"did"`
	Detail    string    `json:"detail,omitempty"`
	Timestamp time.Time `json:"timestamp,omitempty"`
}

type ListDIDResponse struct {
	List []DID `json:"list"`
}

type CreateDidPayload struct {
	KeyType types.KeyType `json:"keyType"`
}

func (p CreateDidPayload) validate() bool {
	return types.ValidateMethod(p.KeyType)
}

func fromSignerDidToKMSDid(from services.ListDidItem) DID {
	return DID{
		Id:        from.Name,
		Did:       from.Did,
		Timestamp: time.Now(),
	}
}

func ListDID(ctx *gin.Context, e common.Env) (any, error) {
	user, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	dids, err := services.GetSigner(e.GetHttpClient()).ListDidDocs(e.GetNamespace(), user.ID())
	if err != nil {
		return nil, err
	}
	var res = ListDIDResponse{List: make([]DID, 0)}
	for _, did := range dids.List {
		res.List = append(res.List, fromSignerDidToKMSDid(did))
	}
	return res, nil
}

func CreateDID(ctx *gin.Context, e common.Env) (any, error) {
	user, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	data, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		return nil, err
	}
	var payload CreateDidPayload
	err = json.Unmarshal(data, &payload)
	if err != nil {
		return nil, err
	}
	if !payload.validate() {
		return nil, fmt.Errorf("invalid key type %s", payload.KeyType)
	}

	cryptoCtx := types.CryptoContext{
		Namespace: e.GetNamespace(),
		Group:     user.ID(),
		Context:   context.Background(),
	}
	var exists bool
	var er error
	if exists, er = e.GetCryptoProvider().IsCryptoContextExisting(cryptoCtx); er == nil && !exists {
		er = e.GetCryptoProvider().CreateCryptoContext(cryptoCtx)
	}
	if er != nil {
		return nil, er
	}

	cryptoId := types.CryptoIdentifier{
		KeyId:         e.GetRandomId(),
		CryptoContext: cryptoCtx,
	}

	cryptoParam := types.CryptoKeyParameter{
		Identifier: cryptoId,
		KeyType:    payload.KeyType,
	}
	err = e.GetCryptoProvider().GenerateKey(cryptoParam)
	if err != nil {
		return nil, err
	}
	return cryptoId.KeyId, nil
}

func DeleteDID(ctx *gin.Context, e common.Env) (any, error) {
	user, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	keyId := ctx.Param("kid")
	cryptoCtx := types.CryptoContext{
		Namespace: e.GetNamespace(),
		Group:     user.ID(),
		Context:   context.Background(),
	}
	var exists bool
	var er error
	if exists, er = e.GetCryptoProvider().IsCryptoContextExisting(cryptoCtx); er == nil && !exists {
		return nil,
			common.ErrorResponse(ctx,
				http.StatusNotFound,
				"provided context does not exist",
				nil)
	}
	if er != nil {
		return nil, er
	}
	cryptoId := types.CryptoIdentifier{
		KeyId:         keyId,
		CryptoContext: cryptoCtx,
	}
	return nil, e.GetCryptoProvider().DeleteKey(cryptoId)
}
