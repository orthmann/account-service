package handlers

import (
	"encoding/json"
	"fmt"
	"io"

	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/model"
)

type SaveConfigrationsRequest struct {
	Language     string `json:"language" binding:"required"`
	HistoryLimit int    `json:"historyLimit" binding:"required"`
}

func GetConfigurations(ctx *gin.Context, e common.Env) (any, error) {
	tmp := ctx.Request.Context().Value(common.UserKey)
	user, ok := tmp.(*common.UserInfo)
	if !ok {
		return nil, common.ErrorResponseBadRequest(ctx, "cannot extract user data from request context", nil)
	}

	db := e.GetDB()

	config, err := model.GetConfigByUserID(db, user.ID())
	if err != nil {
		return nil, common.ErrorResponseBadRequest(ctx, fmt.Sprintf("cannot extract user, %s", err), nil)
	}
	return config.Attributes, nil
}

func SaveConfigurations(ctx *gin.Context, e common.Env) (any, error) {
	tmp := ctx.Request.Context().Value(common.UserKey)
	user, ok := tmp.(*common.UserInfo)
	if !ok {
		return nil, common.ErrorResponseBadRequest(ctx, "cannot extract user data from request context", nil)
	}

	jsonData, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		return nil, common.ErrorResponseBadRequest(ctx, fmt.Sprintf("cannot parse the request, %s", err), nil)
	}

	var req SaveConfigrationsRequest
	err = json.Unmarshal(jsonData, &req)
	if err != nil {
		return nil, common.ErrorResponseBadRequest(ctx, fmt.Sprintf("cannot parse the request, %s", err), nil)
	}

	attributes := map[string]interface{}{
		"language":     req.Language,
		"historyLimit": req.HistoryLimit,
	}
	db := e.GetDB()
	err = model.CreateOrUpdateUserConfigDbEntry(db, user.ID(), attributes)
	if err != nil {
		return nil, common.ErrorResponseBadRequest(ctx, fmt.Sprintf("cannot extract user, %s", err), nil)
	}
	return nil, nil
}
