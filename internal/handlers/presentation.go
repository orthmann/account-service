package handlers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/goccy/go-json"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/ssi/oid4vip/model/presentation"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/model"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/services"
	"io"
	"net/http"
)

type CreateProofPayload struct {
	SignKeyId string `json:"signKeyId"`
	Filters   []presentation.FilterResult
}

func GetPresentationRequest(ctx *gin.Context, e common.Env) (any, error) {
	user, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	presentationId := ctx.Param("id")
	if presentationId == "" {
		return nil, fmt.Errorf("id of presentationRequest not provided")
	}
	err = model.CreatePresentationRequestDBEntry(e.GetDB(), user.ID(), presentationId)
	if err != nil {
		return nil, err
	}
	credentialVerification := services.GetCredentialVerification(e.GetHttpClient())
	request, err := credentialVerification.GetProofRequest(presentationId)
	if err != nil {
		return nil, err
	}
	return services.GetStorage(e.GetHttpClient()).GetCredentials("", user.ID(), &request.PresentationDefinition)
}

func CreatePresentation(ctx *gin.Context, e common.Env) (any, error) {
	user, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	presentationId := ctx.Param("id")
	if presentationId == "" {
		return nil, fmt.Errorf("id of presentationRequest not provided")
	}
	data, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		return nil, err
	}
	var proofPayload CreateProofPayload
	err = json.Unmarshal(data, &proofPayload)
	if err != nil {
		return nil, err
	}
	signer := services.GetSigner(e.GetHttpClient())
	// fetch list of did docs to match the did doc id that matches passed from client
	didObj, err := signer.ListDidDocs(e.GetNamespace(), user.ID())
	if err != nil {
		return nil, err
	}
	var didId = ""
	for _, did := range didObj.List {
		if did.Name == proofPayload.SignKeyId {
			didId = did.Did
		}
	}
	if didId == "" {
		return nil, common.ErrorResponse(ctx, http.StatusNotFound, "did not find DID for provided `signKeyId`", nil)
	}
	// fetch did document
	did, err := signer.GetDidDoc(didId, e.GetNamespace(), user.ID())
	if err != nil {
		return nil, err
	}
	err = services.
		GetCredentialVerification(e.GetHttpClient()).
		CreateProof(presentationId, proofPayload.Filters, e.GetNamespace(), user.ID(), proofPayload.SignKeyId, did)
	if err == nil {
		err = model.DeletePendingRequest(e.GetDB(), user.ID(), presentationId)
	}
	return nil, err
}

func GetPresentationDefinitions(ctx *gin.Context, e common.Env) (any, error) {
	user, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	presentations, err := model.GetAllPresentationRequests(e.GetDB(), user.ID())
	if err != nil {
		return nil, err
	}
	var res = []presentation.PresentationDefinition{}
	for _, pres := range presentations {
		request, err := services.GetCredentialVerification(e.GetHttpClient()).GetProofRequest(pres.RequestId)
		if err != nil {
			return nil, fmt.Errorf("could not get proof request %s", pres.RequestId)
		}
		res = append(res, request.PresentationDefinition)
	}
	return res, nil
}
