package handlers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/libraries/messaging"
	msgCommon "gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/libraries/messaging/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/config"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/services"
	"io"
)

type AcceptRejectOfferPayload struct {
	KeyId string `json:"keyId"`
}

func GetCredentialOffers(ctx *gin.Context, e common.Env) (any, error) {
	user, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	return services.GetCredentialRetrieval(e.GetHttpClient()).GetOffers(user.ID())
}

func CreateCredentialOffer(ctx *gin.Context, e common.Env) (any, error) {
	user, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	data, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		return nil, err
	}
	var offer services.CredentialOfferPayload
	err = json.Unmarshal(data, &offer)
	if err != nil {
		return nil, err
	}
	return nil, services.GetCredentialRetrieval(e.GetHttpClient()).CreateOffer(user.ID(), offer)

}

func AcceptCredentialOffer(ctx *gin.Context, e common.Env) (any, error) {
	return resolveCredentialOffer(ctx, e, true)
}

func DenyCredentialOffer(ctx *gin.Context, e common.Env) (any, error) {
	return resolveCredentialOffer(ctx, e, false)
}

func resolveCredentialOffer(ctx *gin.Context, e common.Env, accept bool) (any, error) {
	user, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	requestId := ctx.Param("id")
	tenantId := ctx.Param("tenantId")

	data, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		return nil, err
	}
	var payload AcceptRejectOfferPayload
	err = json.Unmarshal(data, &payload)
	if err != nil {
		return nil, err
	}

	var eventMsg string
	if accept {
		eventMsg = "Credential offer accepted"
	} else {
		eventMsg = "Credential offer rejected"
	}

	acceptance := messaging.RetrievalAcceptanceNotification{
		Request: msgCommon.Request{
			TenantId:  tenantId,
			RequestId: requestId,
			GroupId:   user.ID(),
		},
		OfferingId:      requestId,
		Message:         eventMsg,
		Result:          accept,
		HolderKey:       payload.KeyId,
		HolderNamespace: e.GetNamespace(),
		HolderGroup:     user.ID(),
	}
	eventData, err := json.Marshal(acceptance)
	if err != nil {
		return nil, err
	}
	accEvent, err := cloudeventprovider.NewEvent(config.ServerConfiguration.Name, common.EventTypeOfferingAcceptance, eventData)
	if err != nil {
		return nil, err
	}
	return nil, e.AddBrokerPublication(config.ServerConfiguration.CredentialRetrival.OfferTopic, accEvent)
}
