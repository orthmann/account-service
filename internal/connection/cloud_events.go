package connection

import (
	"context"
	"fmt"

	"github.com/cloudevents/sdk-go/v2/event"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/config"
)

var logger = common.GetLogger()

func CloudEventsConnectionSubscribe(topic string, handler func(e event.Event)) (*cloudeventprovider.CloudEventProviderClient, func() error, error) {
	// client, err := cloudeventprovider.NewClient(cloudeventprovider.Sub, topic)

	client, err := cloudeventprovider.New(cloudeventprovider.Config{
		Protocol: cloudeventprovider.ProtocolTypeNats,
		Settings: cloudeventprovider.NatsConfig{
			Url:        config.ServerConfiguration.Nats.Url,
			QueueGroup: config.ServerConfiguration.Nats.QueueGroup,
		},
	}, cloudeventprovider.ConnectionTypeSub, topic)

	if err != nil {
		logger.Error(err, "error during processing message")
		return nil, nil, err
	} else {
		logger.Info(fmt.Sprintf("cloudEvents can be received over topic: %s", topic))
	}
	return client, func() error {
		return client.SubCtx(context.TODO(), handler)
	}, nil
}

func CloudEventsConnectionPublish(topic string, e event.Event) (*cloudeventprovider.CloudEventProviderClient, func() error, error) {
	client, err := cloudeventprovider.New(cloudeventprovider.Config{
		Protocol: cloudeventprovider.ProtocolTypeNats,
		Settings: cloudeventprovider.NatsConfig{
			Url:        config.ServerConfiguration.Nats.Url,
			QueueGroup: config.ServerConfiguration.Nats.QueueGroup,
		},
	}, cloudeventprovider.ConnectionTypePub, topic)

	if err != nil {
		logger.Error(err, "error during processing message")
		return nil, nil, err
	} else {
		logger.Info(fmt.Sprintf("cloudEvents can be published to topic: %s", topic))
	}
	return client, func() error {
		return client.PubCtx(context.TODO(), e)
	}, nil
}
